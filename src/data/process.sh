#!/bin/bash

DATA_DIR=raw_data/
PDF_DIR=../../public/pdf/
PROC_DIR=processed_data/
FULL_RB="${PDF_DIR}_FULL_RB/"

# Check if an argument is given
[[ -z $1 ]] && echo "Please provide a mapping file" && exit 1

INPUT=$1
DATA="$DATA_DIR$( cat $INPUT | jq -r '.data' )"
DATA_LEN="$(( $( cat $DATA | wc -l ) - 1 ))"

SLUG="$( cat $INPUT | jq -r '.slug' )"

echo "=== Processing: $( cat $INPUT | jq -r '.name' ) ==="
echo ''

echo -n '' > "${PROC_DIR}$SLUG.csv"

i=1

while read ln
do
  title=$( echo $ln | cut -d ';' -f 1 )
  artist=$( echo $ln | cut -d ';' -f 2 )
  start=$( echo $ln | cut -d ';' -f 3 )
  end=$( echo $ln | cut -d ';' -f 4 )

  [[ $title == 'name' ]] && continue

  echo "$i/$DATA_LEN: $title"

  filename="${SLUG}_$( echo $title | sed "s/ /-/g; s/[\.,-\/\(\)]//g; s/'//g; s/\"//g; s/://g; s/?//g" )_$( echo $artist | sed 's/ \/ /-/g; s/ //g; s/\.//g').pdf"

  keys=""

  while read key
  do
    keystr=$( echo $key | jq -r '.key' )
    offset=$( echo $key | jq -r '.offset' )
    first=$(( start + offset ))
    last=$(( end + offset )) 

    keys="$keys,$keystr"
    
    pdfjam -q "${FULL_RB}$( echo $key | jq -r '.pdf' )" "$first-$last" -o "$PDF_DIR${keystr}_$filename"
  done <<< "$( cat $INPUT | jq -rc '.keys[]' )"

  keys="${keys:1:$( echo $keys | wc -c )}"

  echo "$SLUG;$title;$artist;$filename;$keys" >> "${PROC_DIR}$SLUG.csv"

  ((i=i+1))
done < "$DATA"
