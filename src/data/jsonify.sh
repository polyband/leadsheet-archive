#!/bin/bash

PROC_DATA="processed_data"
JSON=json/data.json
JSON_SRT=json/data_srt.json

echo '[' > $JSON

# i want the sixth edition to be on top
[[ -f $PROC_DATA/R15.csv ]] && mv $PROC_DATA/R15.csv $PROC_DATA/RB1_.csv

for i in $PROC_DATA/*.csv
do
  while read ln
  do
    slug=$( echo $ln | cut -d ';' -f 1 ) 
    name=$( echo $ln | cut -d ';' -f 2 ) 
    artist=$( echo $ln | cut -d ';' -f 3 ) 
    filename=$( echo $ln | cut -d ';' -f 4 ) 
    keys=$( echo $ln | cut -d ';' -f 5 ) 

    keys_json=$( echo $keys | jq -Rr 'split(",")' )
    echo -n "  { \"RB\": \"$slug\", \"name\": \"$name\", \"artist\": \"$artist\", \"filename\": \"$filename\", \"keys\": " >> $JSON
    echo -n $keys_json >> $JSON
    echo " }," >> $JSON
  done < $i
done

echo ']' >> $JSON
# remove last comma of file cuz json
sed -i 'x;${s/,$//;p;x;};1d' $JSON

jq '. | sort_by(.name)' $JSON > $JSON_SRT
